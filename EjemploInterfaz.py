# coding: utf-8

#Imports
import tkinter as tk
from tkinter import ttk

#Ventana raiz
root = tk.Tk()
root.title("Sudoku Solver")
root.resizable(width=False, height=False)
root.configure(padx="10", pady="10")

cuadrante_sudoku = tk.LabelFrame(root)
cuadrante_sudoku.grid(row = 0, column = 0)

#Declaraciones
debug = True
sudoku_original = {}
ingresos = {}
candidatos = {}
numeros_interfaz = []

cuadrantes = []
filas = []
#columnas = []

def obtener_adyacentes(indice):

	if indice == 0 or indice == 3 or indice == 6:
		return[indice + 1, indice + 2]

	if indice == 1 or indice == 4 or indice == 7:
		return[indice - 1, indice + 1]

	if indice == 2 or indice == 5 or indice == 8:
		return[indice - 1, indice - 2]


"""Genera las filas a partir de los cuadrantes
"""
def generar_filas():

	fila_actual = []

	del filas[:]

	filas_cuadrantes = [0, 3, 6]

	for fila_cuadrantes in filas_cuadrantes:

		for contador_fila_actual in range (0, 3):
			fila_actual = []

			for cuadrante in range(fila_cuadrantes, fila_cuadrantes + 3):
				fila_actual.extend(cuadrantes[cuadrante][contador_fila_actual])
			filas.append(fila_actual)


"""Actualiza los cuadrantes a partir de las filas
"""
def actualizar_cuadrantes():

	numeros = []
	del cuadrantes[:]

	cuadrantes.extend(([], [], [], [], [], [], [], [], []))

	filas_cuadrantes = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]

	columnas = [0, 3, 6]

	contador_filas = 0

	for fila_cuadrante in filas_cuadrantes:
		for j in range(contador_filas, contador_filas + 3):
			cuadrantes[fila_cuadrante[0]].append(filas[j][:3])
			cuadrantes[fila_cuadrante[1]].append(filas[j][3:6])
			cuadrantes[fila_cuadrante[2]].append(filas[j][6:9])
		contador_filas += 3

	return(cuadrantes)


"""Actualiza las columnas a partir de las filas
"""
def actualizar_columnas():

	columnas = []

	for j in range (0, 9):
		columna = []

		for fila in filas:
			columna.append(fila[j])

		columnas.append(columna)

	return(columnas)


def completar():

	numeros_posibles = [1, 2, 3, 4, 5, 6, 7, 8, 9]

	#completar las filas

	for fila in filas:

		if fila.count(0) == 1:
			#print("Faltante")
			
			for numero in numeros_posibles:
				if fila.count(numero) == 0:
					#print(fila)
					#print("Falta el %d" % numero)
					fila.insert(fila.index(0), numero)
					fila.remove(0)
					#print(fila)
					#print()


	#completar las columnas

	columnas = actualizar_columnas()

	for j, columna in enumerate(columnas):

		if columna.count(0) == 1:
			posicion = columna.index(0)
			print("Faltante en fila %d" % posicion)

			for numero in numeros_posibles:
				if columna.count(numero) == 0:
					#print(columna)
					#print("Falta el %d" % numero)
					#print(j)
					filas[posicion].insert(j + 1, numero)
					filas[posicion].remove(0)
					#print()

	#completar los cuadrantes

	cuadrantes = actualizar_cuadrantes()

	for k, cuadrante in enumerate(cuadrantes):
		faltantes = 0
		#print(cuadrante)

		posicion_completable = -1
		fila_completable = -1
		numeros_en_fila = []

		for fila_actual, fila in enumerate(cuadrante):
			
			numeros_en_fila.extend(fila)

			if fila.count(0) == 1:
				faltantes += 1
				posicion_completable = fila.index(0)
				fila_completable = fila_actual

		if faltantes == 1:
			
			for numero in numeros_posibles:

				if numero not in numeros_en_fila:
					cuadrante[fila_completable].insert((posicion_completable + 1), numero)
					cuadrante[fila_completable].pop(posicion_completable)
					break

	generar_filas()

	escribir()


def posicion_unica():

	numeros_posibles = [1, 2, 3, 4, 5, 6, 7, 8, 9]

	cuadrantes = actualizar_cuadrantes()

	for indice, fila in enumerate(filas):
		
		adyacentes = obtener_adyacentes(indice)
		
		for numero in numeros_posibles:

			if numero not in fila:
				if (numero in filas[adyacentes[0]] and 
					numero in filas[adyacentes[1]]):

					columnas = actualizar_columnas()
					
					for indice_col, columna in enumerate(columnas):

						adyacentes = obtener_adyacentes(indice_col)
						if numero not in columna:
							if (numero in columnas[adyacentes[0]] and 
								numero in columnas[adyacentes[1]]):
								print('Posicion unica encontrada: %d en fila %d y columna %d' % (numero, indice, indice_col))


def escribir():

	print()
	for i, fila in enumerate(filas):
		print(str(fila[0:3]) + ' | ' + str(fila[3:6]) + ' | ' + str(fila[6:9]))
		
		if i == 2 or i == 5:
			print('-' * 33)


def leer():

	numeros = []

	del cuadrantes[:]

	for numero_entrada in numeros_interfaz:

		if numero_entrada.get():
			numeros.append(int(numero_entrada.get()))
		else:
			numeros.append(0)

	numeros.reverse()

	for k in range (0, 9):

		cuadrante = []

		for i in range (0, 3):
			
			fila = []
		
			for x in range (0, 3):
				
				fila.append(numeros.pop())
			
			cuadrante.append(fila)

		cuadrantes.append(cuadrante)

	#Generar las filas

	generar_filas()


def interfaz_lectura():

	valores = ['', 1, 2, 3, 4, 5, 6, 7, 8, 9]	
	cuadrantes_interfaz = []
	columna = 0
	fila = 0

	for k in range (0, 9):
	
		cuadrante = tk.LabelFrame(
			cuadrante_sudoku,
			width = 150,
			height = 150,
			bd = 2,
			background = '#FFFFFF')

		cuadrantes_interfaz.append(cuadrante)

		for i in range (0, 3):

			for j in range (0, 3):

				numero = ttk.Combobox(
					cuadrante,
					font = 'bold',
					values = valores,
					state = 'readonly',
					width = 2)

				numeros_interfaz.append(numero)

				numero.grid(row = i, column = j, padx = 10, pady = 10)

		cuadrante.grid(row = fila, column = columna, sticky = "n")

		columna += 1

		if columna == 3:
			columna = 0
			fila += 1


	btn_leer = tk.Button(cuadrante_sudoku,
				text = "Leer Sudoku",
				command = lambda: leer())

	btn_solucionar = tk.Button(cuadrante_sudoku,
				text = "Solucionar",
				command = lambda: interfaz_resultados())

	btn_leer.grid(row = 3, column = 0, pady = (10, 10), padx = 10)
	btn_solucionar.grid(row = 3, column = 2, pady = (10, 10), padx = 10)


def interfaz_resultados():
	
	if not debug:
		for numero in numeros_interfaz:
			numero.grid_forget()


interfaz_lectura()


"""Interfaz de debug
"""
if debug:

	cuadrante_funciones = tk.LabelFrame(root)
	cuadrante_funciones.grid(row = 0, column = 1, padx = (10, 0))

	btn_completar = tk.Button(cuadrante_funciones,
			text = "Completar Sudoku",
			command = lambda: completar())
	btn_completar.grid(row = 0, column = 0, pady = (10, 10), padx = 10)

	btn_posicion_unica = tk.Button(cuadrante_funciones,
			text = "Posicion Unica",
			command = lambda: posicion_unica())
	btn_posicion_unica.grid(row = 1, column = 0, pady = (10, 10), padx = 10)

root.mainloop()